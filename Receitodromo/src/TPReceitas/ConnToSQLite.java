package TPReceitas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**Classe ConnToSQLite

 * @author Nelson Silva e Michael Luis

 * @version 1.2

 */

public class ConnToSQLite {

	public static final String JDBC_DRIVER = "org.sqlite.JDBC";
	public static final String DB_URL = "jdbc:sqlite:C://Users//nelso//git//receitodromo//Receitodromo//receitodromoBD.sqlite";
	
	static Connection conn = null;
	
	
	   /** M�todo para criar a conex�o � base de dados*/
	
	public static void criarConn() {
		
	// INICIO BD
			try {
				Class.forName(JDBC_DRIVER);
				System.out.println("A conectar � base de dados");
				conn = DriverManager.getConnection(DB_URL);

			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} finally {
				
			}
			// FIM DB
	}

	   /** M�todo para retorno da conex�o � base de dados local

     *   @return Connection - conn*/
	
	public static Connection getConn() {
		return conn;
	}

	   /** M�todo para alterar o url da base de dados */
	
	public static void setConn(Connection conn) {
		ConnToSQLite.conn = conn;
	}
	
}
