package TPReceitas;

//Imports
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.GridBagLayout;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Toolkit;

/**Classe MainMenu

 * @author Nelson Silva e Michael Luis

 * @version 1.2

 */

public class MainMenu extends JFrame {

	// JDBC driver e URL da base de dados

	private JPanel contentPane;
	private JTable tableReceitas;
	private JTable tableIngredientes;
	private JTable tableIngredientesExistentes;
	private JTable tableIngredientesEscolhidos;
	private JTextField textFieldQtd;
	private JTextField textFieldNomeReceita;
	private JTextField textFieldDuracaoReceita;
	private JTextField textFieldConfecaoReceita;
	private JTextField textFieldNDosesReceitas;
	private JTextField textFieldNomeIngrediente;
	private JTextField textFieldDescricaoIngrediente;
	private JTextField textFieldCaloriasIngrediente;
	private JTable tableIngredientesExistente;

	ConnToSQLite sqlite = new ConnToSQLite();
	Statement stmt = null;
	ResultSet result = null;
	Connection conn = null;
	private JTable tableEditIngredientesExistentes;
	private JTable tableEditIngredientesEscolhidos;
	private JTextField textFieldEditQtd;
	private JTextField textFieldEditNomeReceita;
	private JTextField textFieldEditDuracaoReceita;
	private JTextField textFieldEditConfecaoReceita;
	private JTextField textFieldEditNDosesReceita;
	private JTable tableReceitaPesquisa;
	private JTable tableIngredientesPesquisa;
	private JTextField textFieldNomePesquisa;
	private JTextField textFieldCaloriasPesquisa;
	private JTextField textFieldNDosesPesquisa;

	/**
	 * Launch the application.
	 */
	// Classe main
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainMenu frame = new MainMenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	// Classe principal
	public MainMenu() {
		setIconImage(
				Toolkit.getDefaultToolkit().getImage("C:\\Users\\nelso\\git\\receitodromo\\Receitodromo\\icontp.png"));
		setTitle("Receitodromo");

		// Criar a conex�o na classe ConnToSQLite
		sqlite.criarConn();
		conn = sqlite.getConn();

		// Configuracao das propriedades da janela
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		// Inicio da declaracao do panelPesquisa -
		JPanel panelPesquisa = new JPanel();
		panelPesquisa.setBounds(0, 0, 784, 561);
		contentPane.add(panelPesquisa);
		panelPesquisa.setLayout(null);

		// Declara��o do scrollPane da tableReceitaPesquisa
		JScrollPane scrollPane_6 = new JScrollPane();
		scrollPane_6.setBounds(10, 30, 616, 244);
		panelPesquisa.add(scrollPane_6);

		// Declara��o da table tableReceitaPesquisa
		tableReceitaPesquisa = new JTable();
		scrollPane_6.setViewportView(tableReceitaPesquisa);

		// Declara��o do scrollPane da tableIngredientesPesquisa
		JScrollPane scrollPane_7 = new JScrollPane();
		scrollPane_7.setBounds(10, 306, 426, 244);
		panelPesquisa.add(scrollPane_7);

		// Declara��o da table tableIngredientesPesquisa
		tableIngredientesPesquisa = new JTable();
		scrollPane_7.setViewportView(tableIngredientesPesquisa);
		DefaultTableModel tableIngredientesPesquisaModel = (DefaultTableModel) tableIngredientesPesquisa.getModel();
		tableIngredientesPesquisaModel.addColumn("Nome");
		tableIngredientesPesquisaModel.addColumn("Quantidade");
		tableIngredientesPesquisaModel.addColumn("Unidade");
		tableIngredientesPesquisa.setDefaultEditor(Object.class, null);

		// Declara��o do scrollPane da textAreaPassosPesquisa
		JScrollPane scrollPane_8 = new JScrollPane();
		scrollPane_8.setBounds(446, 306, 328, 244);
		panelPesquisa.add(scrollPane_8);

		// Declara��o da textArea textAreaPassosPesquisa
		JTextArea textAreaPassosPesquisa = new JTextArea();
		scrollPane_8.setViewportView(textAreaPassosPesquisa);

		// Declara��o da label lblIngredientesPesquisa
		JLabel lblIngredientesPesquisa = new JLabel("Ingredientes");
		lblIngredientesPesquisa.setBounds(10, 285, 125, 14);
		panelPesquisa.add(lblIngredientesPesquisa);

		// Declara��o da label lblPassosPesquisa
		JLabel lblPassosPesquisa = new JLabel("Passos");
		lblPassosPesquisa.setBounds(446, 285, 46, 14);
		panelPesquisa.add(lblPassosPesquisa);

		// Declara��o do button btnPesquisar
		JButton btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.setBounds(650, 233, 116, 23);
		panelPesquisa.add(btnPesquisar);

		// Declara��o da textField textFieldNomePesquisa
		textFieldNomePesquisa = new JTextField();
		textFieldNomePesquisa.setBounds(677, 30, 101, 20);
		panelPesquisa.add(textFieldNomePesquisa);
		textFieldNomePesquisa.setColumns(10);

		// Declara��o da comboBox comboBoxTipoPesquisa
		JComboBox comboBoxTipoPesquisa = new JComboBox();
		comboBoxTipoPesquisa.setBounds(677, 177, 101, 20);
		panelPesquisa.add(comboBoxTipoPesquisa);
		DefaultComboBoxModel listComboBoxTipoPesquisa = new DefaultComboBoxModel();
		comboBoxTipoPesquisa.setModel(listComboBoxTipoPesquisa);

		// Declara��o da comboBox comboBoxClassificacaoPesquisa
		JComboBox comboBoxClassificacaoPesquisa = new JComboBox();
		comboBoxClassificacaoPesquisa.setBounds(725, 202, 53, 20);
		comboBoxClassificacaoPesquisa.addItem("Todos");
		comboBoxClassificacaoPesquisa.addItem(1);
		comboBoxClassificacaoPesquisa.addItem(2);
		comboBoxClassificacaoPesquisa.addItem(3);
		comboBoxClassificacaoPesquisa.addItem(4);
		comboBoxClassificacaoPesquisa.addItem(5);
		panelPesquisa.add(comboBoxClassificacaoPesquisa);

		// Declara��o da textField textFieldCaloriasPesquisa
		textFieldCaloriasPesquisa = new JTextField();
		textFieldCaloriasPesquisa.setColumns(10);
		textFieldCaloriasPesquisa.setBounds(640, 147, 138, 20);
		panelPesquisa.add(textFieldCaloriasPesquisa);

		// Declara��o da textField textFieldNDosesPesquisa
		textFieldNDosesPesquisa = new JTextField();
		textFieldNDosesPesquisa.setColumns(10);
		textFieldNDosesPesquisa.setBounds(640, 91, 138, 20);
		panelPesquisa.add(textFieldNDosesPesquisa);

		// Declara��o da label lblNomePesquisa
		JLabel lblNomePesquisa = new JLabel("Nome:");
		lblNomePesquisa.setBounds(636, 33, 46, 14);
		panelPesquisa.add(lblNomePesquisa);

		// Declara��o da label lblNDosesPesquisa
		JLabel lblNDosesPesquisa = new JLabel("N\u00BA Doses:");
		lblNDosesPesquisa.setBounds(636, 64, 68, 14);
		panelPesquisa.add(lblNDosesPesquisa);

		// Declara��o da label lblCaloriasPesquisa
		JLabel lblCaloriasPesquisa = new JLabel("Calorias:");
		lblCaloriasPesquisa.setBounds(640, 122, 64, 14);
		panelPesquisa.add(lblCaloriasPesquisa);

		// Declara��o da label lblTipoPesquisa
		JLabel lblTipoPesquisa = new JLabel("Tipo:");
		lblTipoPesquisa.setBounds(640, 180, 46, 14);
		panelPesquisa.add(lblTipoPesquisa);

		// Declara��o da label lblClassificacaoPesquisa
		JLabel lblClassificacaoPesquisa = new JLabel("Classificacao:");
		lblClassificacaoPesquisa.setBounds(640, 205, 84, 14);
		panelPesquisa.add(lblClassificacaoPesquisa);

		// Declara��o da comboBox comboBoxNDosesPesquisa
		JComboBox comboBoxNDosesPesquisa = new JComboBox();
		comboBoxNDosesPesquisa.setBounds(699, 61, 79, 20);
		comboBoxNDosesPesquisa.addItem("<=");
		comboBoxNDosesPesquisa.addItem(">=");
		panelPesquisa.add(comboBoxNDosesPesquisa);

		// Declara��o da comboBox comboBoxCaloriasPesquisa
		JComboBox comboBoxCaloriasPesquisa = new JComboBox();
		comboBoxCaloriasPesquisa.setBounds(699, 119, 79, 20);
		comboBoxCaloriasPesquisa.addItem("<=");
		comboBoxCaloriasPesquisa.addItem(">=");
		panelPesquisa.add(comboBoxCaloriasPesquisa);

		// Declara��o do button buttonCancelarPesquisa
		JButton buttonCancelarPesquisa = new JButton("Cancelar");
		buttonCancelarPesquisa.setBounds(650, 267, 116, 23);
		panelPesquisa.add(buttonCancelarPesquisa);

		// Declara��o da label lblReceitasPesquisa
		JLabel lblReceitasPesquisa = new JLabel("Receitas");
		lblReceitasPesquisa.setBounds(10, 11, 84, 14);
		panelPesquisa.add(lblReceitasPesquisa);

		// Inicio da declaracao do painelEditReceit -
		JPanel panelEditReceita = new JPanel();
		panelEditReceita.setBounds(0, 0, 784, 561);
		contentPane.add(panelEditReceita);
		panelEditReceita.setLayout(null);

		// Declara��o do scrollPane da tableEditIngredientesExistentes
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(20, 11, 279, 243);
		panelEditReceita.add(scrollPane_3);

		// Declara��o da tabela tableEditIngredientesExistentes
		tableEditIngredientesExistentes = new JTable();
		tableEditIngredientesExistentes.setDefaultEditor(Object.class, null);
		scrollPane_3.setViewportView(tableEditIngredientesExistentes);

		// Declara��o do scrollPane da tableEditIngredientesEscolhidos
		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(484, 11, 279, 243);
		panelEditReceita.add(scrollPane_4);

		// Declara��o da tabela tableEditIngredientesEscolhidos
		tableEditIngredientesEscolhidos = new JTable();
		DefaultTableModel tableEditIngredientesEscolhidosCriacaoModel = (DefaultTableModel) tableEditIngredientesEscolhidos
				.getModel();
		tableEditIngredientesEscolhidosCriacaoModel.addColumn("Nome");
		tableEditIngredientesEscolhidosCriacaoModel.addColumn("Calorias");
		tableEditIngredientesEscolhidosCriacaoModel.addColumn("Desc");
		tableEditIngredientesEscolhidosCriacaoModel.addColumn("Qtd");
		tableEditIngredientesEscolhidosCriacaoModel.addColumn("Unidade");
		tableEditIngredientesEscolhidos.setDefaultEditor(Object.class, null);
		scrollPane_4.setViewportView(tableEditIngredientesEscolhidos);

		// Declara��o do button btnEditAdicionaIngrediente
		JButton btnEditAdicionaIngrediente = new JButton(">");
		btnEditAdicionaIngrediente.setBounds(321, 86, 134, 23);
		panelEditReceita.add(btnEditAdicionaIngrediente);

		// Declara��o da button btnEditRemoveIngrediente
		JButton btnEditRemoveIngrediente = new JButton("<");
		btnEditRemoveIngrediente.setBounds(321, 120, 134, 23);
		panelEditReceita.add(btnEditRemoveIngrediente);

		// Declara��o do textField textFieldEditQtd
		textFieldEditQtd = new JTextField();
		textFieldEditQtd.setBounds(347, 154, 108, 20);
		panelEditReceita.add(textFieldEditQtd);
		textFieldEditQtd.setColumns(10);

		// Declara��o da label lblEditQtd
		JLabel lblEditQtd = new JLabel("Qtd: ");
		lblEditQtd.setBounds(321, 157, 46, 14);
		panelEditReceita.add(lblEditQtd);

		// Declara��o do textField textFieldEditNomeReceita
		textFieldEditNomeReceita = new JTextField();
		textFieldEditNomeReceita.setBounds(108, 281, 491, 33);
		panelEditReceita.add(textFieldEditNomeReceita);
		textFieldEditNomeReceita.setColumns(10);

		// Declara��o do textField textFieldEditDuracaoReceita
		textFieldEditDuracaoReceita = new JTextField();
		textFieldEditDuracaoReceita.setBounds(108, 369, 190, 33);
		panelEditReceita.add(textFieldEditDuracaoReceita);
		textFieldEditDuracaoReceita.setColumns(10);

		// Declara��o da label lblEditNomeReceita
		JLabel lblEditNomeReceita = new JLabel("Nome:");
		lblEditNomeReceita.setBounds(40, 290, 46, 14);
		panelEditReceita.add(lblEditNomeReceita);

		// Declara��o do scrollPane da textAreaEditPassosReceita
		JScrollPane scrollPane_5 = new JScrollPane();
		scrollPane_5.setBounds(108, 457, 491, 87);
		panelEditReceita.add(scrollPane_5);

		// Declara��o da textArea textAreaEditPassosReceita
		JTextArea textAreaEditPassosReceita = new JTextArea();
		scrollPane_5.setViewportView(textAreaEditPassosReceita);

		// Declara��o da label lblEditDuracaoReceita
		JLabel lblEditDuracaoReceita = new JLabel("Dura\u00E7\u00E3o:");
		lblEditDuracaoReceita.setBounds(40, 378, 58, 14);
		panelEditReceita.add(lblEditDuracaoReceita);

		// Declara��o da label lblEditPassosReceita
		JLabel lblEditPassosReceita = new JLabel("Passos:");
		lblEditPassosReceita.setBounds(40, 457, 46, 14);
		panelEditReceita.add(lblEditPassosReceita);

		// Declara��o da label lblEditTipoReceita
		JLabel lblEditTipoReceita = new JLabel("Tipo:");
		lblEditTipoReceita.setBounds(40, 422, 46, 14);
		panelEditReceita.add(lblEditTipoReceita);

		// Declara��o da comboBox comboBoxEditTipoReceita
		JComboBox comboBoxEditTipoReceita = new JComboBox();
		comboBoxEditTipoReceita.setBounds(108, 413, 190, 33);
		panelEditReceita.add(comboBoxEditTipoReceita);
		DefaultComboBoxModel listComboBoxEditTipoReceita = new DefaultComboBoxModel();
		comboBoxEditTipoReceita.setModel(listComboBoxEditTipoReceita);

		// Declara��o da button btnEditGuardaReceita
		JButton btnEditGuardaReceita = new JButton("Guardar");
		btnEditGuardaReceita.setBounds(645, 325, 108, 33);
		panelEditReceita.add(btnEditGuardaReceita);

		// Declara��o da button btnEditLimpaReceita
		JButton btnEditLimpaReceita = new JButton("Limpar");
		btnEditLimpaReceita.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				LimparEditTabelaReceitas();
				textAreaEditPassosReceita.setText("");
			}
		});
		btnEditLimpaReceita.setBounds(645, 369, 108, 33);
		panelEditReceita.add(btnEditLimpaReceita);

		// Declara��o da button btnEditCancelaReceita
		JButton btnEditCancelaReceita = new JButton("Cancelar");
		btnEditCancelaReceita.setBounds(645, 413, 108, 33);
		panelEditReceita.add(btnEditCancelaReceita);

		// Declara��o do textField textFieldEditConfecaoReceita
		textFieldEditConfecaoReceita = new JTextField();
		textFieldEditConfecaoReceita.setBounds(403, 369, 196, 33);
		panelEditReceita.add(textFieldEditConfecaoReceita);
		textFieldEditConfecaoReceita.setColumns(10);

		// Declara��o da label lblEditNDosesReceita
		JLabel lblEditNDosesReceita = new JLabel("N\u00BA Doses:");
		lblEditNDosesReceita.setBounds(40, 334, 58, 14);
		panelEditReceita.add(lblEditNDosesReceita);

		// Declara��o da label lblConfecaoReceita
		JLabel lblEditConfecaoReceita = new JLabel("Confe\u00E7\u00E3o:");
		lblEditConfecaoReceita.setBounds(308, 378, 58, 14);
		panelEditReceita.add(lblEditConfecaoReceita);

		// Declara��o da label lblConfecaoReceita
		JLabel lblEditClassificacaoReceita = new JLabel("Classifica\u00E7\u00E3o:");
		lblEditClassificacaoReceita.setBounds(309, 422, 84, 14);
		panelEditReceita.add(lblEditClassificacaoReceita);

		// Declara��o da comboBox comboBoxEditClassificacaoReceeita
		JComboBox comboBoxEditClassificacaoReceeita = new JComboBox();
		comboBoxEditClassificacaoReceeita.setBounds(403, 413, 196, 33);
		comboBoxEditClassificacaoReceeita.addItem(1);
		comboBoxEditClassificacaoReceeita.addItem(2);
		comboBoxEditClassificacaoReceeita.addItem(3);
		comboBoxEditClassificacaoReceeita.addItem(4);
		comboBoxEditClassificacaoReceeita.addItem(5);
		panelEditReceita.add(comboBoxEditClassificacaoReceeita);

		// Declara��o da label lblEditIDReceita
		JLabel lblEditIDReceita = new JLabel("");
		lblEditIDReceita.setEnabled(false);
		lblEditIDReceita.setBounds(40, 265, 46, 14);
		panelEditReceita.add(lblEditIDReceita);
		lblEditIDReceita.setVisible(false);

		// Declara��o do textField textFieldNDosesReceitas
		textFieldEditNDosesReceita = new JTextField();
		textFieldEditNDosesReceita.setBounds(108, 325, 491, 33);
		panelEditReceita.add(textFieldEditNDosesReceita);
		textFieldEditNDosesReceita.setColumns(10);

		// Declara��o da comboBoxEditUnidade
		JComboBox comboBoxEditUnidade = new JComboBox();
		comboBoxEditUnidade.addItem("Kg");
		comboBoxEditUnidade.addItem("L");
		comboBoxEditUnidade.addItem("ml");
		comboBoxEditUnidade.addItem("g");
		comboBoxEditUnidade.addItem("Unid");
		comboBoxEditUnidade.setBounds(384, 182, 72, 20);
		panelEditReceita.add(comboBoxEditUnidade);

		JLabel labelEditUnidades = new JLabel("Unidade");
		labelEditUnidades.setBounds(321, 185, 72, 14);
		panelEditReceita.add(labelEditUnidades);

		// Inicio da declaracao do painel principal
		JPanel panelMain = new JPanel();
		panelMain.setBounds(0, 0, 784, 561);
		contentPane.add(panelMain);
		panelMain.setLayout(null);

		// Declara��o do scrollPane da tableReceitas
		JScrollPane scrollPaneTableReceitas = new JScrollPane();
		scrollPaneTableReceitas.setBounds(10, 30, 616, 244);
		panelMain.add(scrollPaneTableReceitas);

		// Declara��o da tabela tableReceitas
		tableReceitas = new JTable();
		scrollPaneTableReceitas.setViewportView(tableReceitas);

		// Declara��o do scrollPane da tableIngredientes
		JScrollPane scrollPaneTableIngredientes = new JScrollPane();
		scrollPaneTableIngredientes.setBounds(10, 306, 426, 244);
		panelMain.add(scrollPaneTableIngredientes);

		// Declara��o da tabela tableIngredientes
		tableIngredientes = new JTable();
		scrollPaneTableIngredientes.setViewportView(tableIngredientes);
		DefaultTableModel tableIngredientesModel = (DefaultTableModel) tableIngredientes.getModel();
		tableIngredientesModel.addColumn("Nome");
		tableIngredientesModel.addColumn("Quantidade");
		tableIngredientesModel.addColumn("Unidade");
		tableIngredientes.setDefaultEditor(Object.class, null);

		// Declara��o do scrollPane da textAreaPassosReceitasMain
		JScrollPane scrollPaneListaPreparacao = new JScrollPane();
		scrollPaneListaPreparacao.setBounds(446, 306, 328, 244);
		panelMain.add(scrollPaneListaPreparacao);

		// Declara��o do textAreaPassosReceitasMain
		JTextArea textAreaPassosReceitasMain = new JTextArea();
		textAreaPassosReceitasMain.setEditable(false);
		scrollPaneListaPreparacao.setViewportView(textAreaPassosReceitasMain);

		// Declara��o do button buttonAddReceita
		JButton buttonAddReceita = new JButton("Adicionar Receita");
		buttonAddReceita.setBounds(636, 76, 138, 23);
		panelMain.add(buttonAddReceita);

		// Declara��o do button btnDeleteReceita
		JButton btnDeleteReceita = new JButton("Apagar Receita");
		btnDeleteReceita.setBounds(636, 110, 138, 23);
		panelMain.add(btnDeleteReceita);

		// Declara��o do button buttonPesquisa
		JButton buttonPesquisa = new JButton("Pesquisa");
		buttonPesquisa.setBounds(636, 212, 138, 23);
		panelMain.add(buttonPesquisa);

		// Declara��o do button btnEditReceita
		JButton btnEditReceita = new JButton("Editar Receita");
		btnEditReceita.setBounds(636, 144, 138, 23);
		panelMain.add(btnEditReceita);

		// Declara��o do button btnIngredientes
		JButton btnIngredientes = new JButton("Ingredientes");
		btnIngredientes.setBounds(636, 178, 139, 23);
		panelMain.add(btnIngredientes);

		// Declara��o da label lblReceitasMain
		JLabel lblReceitasMain = new JLabel("Receitas");
		lblReceitasMain.setBounds(10, 11, 84, 14);
		panelMain.add(lblReceitasMain);

		// Declara��o do label lblIngredientesMain
		JLabel lblIngredientesMain = new JLabel("Ingredientes");
		lblIngredientesMain.setBounds(10, 285, 125, 14);
		panelMain.add(lblIngredientesMain);

		// Declara��o do label lblPassosMain
		JLabel lblPassosMain = new JLabel("Passos");
		lblPassosMain.setBounds(446, 285, 46, 14);
		panelMain.add(lblPassosMain);

		// Inicio da declaracao do painel relacionado com os Ingredientes -
		JPanel panelIngredientes = new JPanel();
		panelIngredientes.setBounds(0, 0, 794, 561);
		contentPane.add(panelIngredientes);
		panelIngredientes.setLayout(null);

		// Declara��o do textField textFieldNomeIngrediente
		textFieldNomeIngrediente = new JTextField();
		textFieldNomeIngrediente.setColumns(10);
		textFieldNomeIngrediente.setBounds(94, 400, 400, 33);
		panelIngredientes.add(textFieldNomeIngrediente);

		// Declara��o do textField textFieldDescricaoIngrediente
		textFieldDescricaoIngrediente = new JTextField();
		textFieldDescricaoIngrediente.setColumns(10);
		textFieldDescricaoIngrediente.setBounds(94, 447, 400, 33);
		panelIngredientes.add(textFieldDescricaoIngrediente);

		// Declara��o do textField textFieldCaloriasIngredientes
		textFieldCaloriasIngrediente = new JTextField();
		textFieldCaloriasIngrediente.setColumns(10);
		textFieldCaloriasIngrediente.setBounds(94, 491, 400, 33);
		panelIngredientes.add(textFieldCaloriasIngrediente);

		// Declara��o da label lblIngredientesExistentes
		JLabel lblIngredientesExistentes = new JLabel("Ingrediente Existentes");
		lblIngredientesExistentes.setBounds(10, 11, 138, 14);
		panelIngredientes.add(lblIngredientesExistentes);

		// Declara��o da label lblNomeIngrediente
		JLabel lblNomeIngrediente = new JLabel("Nome:");
		lblNomeIngrediente.setBounds(28, 409, 46, 14);
		panelIngredientes.add(lblNomeIngrediente);

		// Declara��o da label lblDescricaoIngrediente
		JLabel lblDescricaoIngrediente = new JLabel("Descri\u00E7\u00E3o:");
		lblDescricaoIngrediente.setBounds(28, 456, 82, 14);
		panelIngredientes.add(lblDescricaoIngrediente);

		// Declara��o da label lblCaloriasIngrediente
		JLabel lblCaloriasIngrediente = new JLabel("Calorias:");
		lblCaloriasIngrediente.setBounds(28, 500, 56, 14);
		panelIngredientes.add(lblCaloriasIngrediente);

		// Declara��o da label lblAdicionarIngrediente
		JLabel lblAdicionarIngrediente = new JLabel("Adicionar Ingrediente");
		lblAdicionarIngrediente.setBounds(10, 363, 138, 14);
		panelIngredientes.add(lblAdicionarIngrediente);

		// Declara��o do scrollPane da table IngredientesExistente
		JScrollPane scrollPaneIngredienteExistentes = new JScrollPane();
		scrollPaneIngredienteExistentes.setEnabled(false);
		scrollPaneIngredienteExistentes.setBounds(10, 36, 774, 301);
		panelIngredientes.add(scrollPaneIngredienteExistentes);

		// Declara��o da tabela tableIngredientesExistente
		tableIngredientesExistente = new JTable();
		tableIngredientesExistente.setDefaultEditor(Object.class, null);
		scrollPaneIngredienteExistentes.setViewportView(tableIngredientesExistente);

		// Declara��o do button buttonGuardarIngredientes
		JButton buttonGuardarIngredientes = new JButton("Guardar");
		buttonGuardarIngredientes.setBounds(598, 429, 108, 33);
		panelIngredientes.add(buttonGuardarIngredientes);

		// Declara��o do button buttonLimparIngrediente
		JButton buttonLimparIngrediente = new JButton("Limpar");
		buttonLimparIngrediente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				LimparTabelaIngredientes();
			}
		});
		buttonLimparIngrediente.setBounds(598, 473, 108, 33);
		panelIngredientes.add(buttonLimparIngrediente);

		// Declara��o do button buttonCancelarIngrediente
		JButton buttonCancelarIngrediente = new JButton("Cancelar");
		buttonCancelarIngrediente.setBounds(598, 517, 108, 33);
		panelIngredientes.add(buttonCancelarIngrediente);

		// Declara��o do button buttonEliminarIngrediente
		JButton buttonEliminarIngrediente = new JButton("Eliminar");
		buttonEliminarIngrediente.setBounds(598, 385, 108, 33);
		panelIngredientes.add(buttonEliminarIngrediente);

		// Inicio da declaracao do painel relacionado com as receitas -
		JPanel panelReceita = new JPanel();
		panelReceita.setBounds(0, 0, 784, 561);
		contentPane.add(panelReceita);
		panelReceita.setLayout(null);

		// Declara��o do scrollPane da tableIngredientesExistentes
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		scrollPane.setBounds(20, 11, 279, 243);
		panelReceita.add(scrollPane);

		// Declara��o da tabela tableIngredientesExistentes
		tableIngredientesExistentes = new JTable();
		tableIngredientesExistentes.setDefaultEditor(Object.class, null);
		scrollPane.setViewportView(tableIngredientesExistentes);

		// Declara��o do scrollPane da tableIngredientesEscolhidos
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(484, 11, 279, 243);
		panelReceita.add(scrollPane_1);

		// Declara��o da tabela tableIngredientesEscolhidos
		tableIngredientesEscolhidos = new JTable();
		DefaultTableModel tableIngredientesEscolhidosCriacaoModel = (DefaultTableModel) tableIngredientesEscolhidos
				.getModel();
		tableIngredientesEscolhidosCriacaoModel.addColumn("Nome");
		tableIngredientesEscolhidosCriacaoModel.addColumn("Calorias");
		tableIngredientesEscolhidosCriacaoModel.addColumn("Desc");
		tableIngredientesEscolhidosCriacaoModel.addColumn("Qtd");
		tableIngredientesEscolhidosCriacaoModel.addColumn("Unidade");
		tableIngredientesEscolhidos.setDefaultEditor(Object.class, null);
		scrollPane_1.setViewportView(tableIngredientesEscolhidos);

		// Declara��o do button btnAdicionaIngrediente
		JButton btnAdicionaIngrediente = new JButton(">");
		btnAdicionaIngrediente.setBounds(321, 86, 134, 23);
		panelReceita.add(btnAdicionaIngrediente);

		// Declara��o do button btnRemoveIngrediente
		JButton btnRemoveIngrediente = new JButton("<");
		btnRemoveIngrediente.setBounds(321, 120, 134, 23);
		panelReceita.add(btnRemoveIngrediente);

		// Declara��o do textField textFieldQtd
		textFieldQtd = new JTextField();
		textFieldQtd.setBounds(347, 154, 108, 20);
		panelReceita.add(textFieldQtd);
		textFieldQtd.setColumns(10);

		// Declara��o da label lblQtd
		JLabel lblQtd = new JLabel("Qtd: ");
		lblQtd.setBounds(321, 157, 46, 14);
		panelReceita.add(lblQtd);

		// Declara��o do textField textFieldNomeReceita
		textFieldNomeReceita = new JTextField();
		textFieldNomeReceita.setBounds(108, 281, 491, 33);
		panelReceita.add(textFieldNomeReceita);
		textFieldNomeReceita.setColumns(10);

		// Declara��o da label lblNomeReceita
		JLabel lblNomeReceita = new JLabel("Nome:");
		lblNomeReceita.setBounds(40, 290, 46, 14);
		panelReceita.add(lblNomeReceita);

		// Declara��o do textField textFieldDuracaoReceita
		textFieldDuracaoReceita = new JTextField();
		textFieldDuracaoReceita.setColumns(10);
		textFieldDuracaoReceita.setBounds(108, 369, 190, 33);
		panelReceita.add(textFieldDuracaoReceita);

		// Declara��o do scrollPane da textAreaPassosReceita
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(108, 457, 491, 87);
		panelReceita.add(scrollPane_2);

		// Declara��o da textArea textAreaPassosReceita
		JTextArea textAreaPassosReceita = new JTextArea();
		scrollPane_2.setViewportView(textAreaPassosReceita);

		// Declara��o da label lblDuracaoReceita
		JLabel lblDuracaoReceita = new JLabel("Dura\u00E7\u00E3o:");
		lblDuracaoReceita.setBounds(40, 378, 58, 14);
		panelReceita.add(lblDuracaoReceita);

		// Declara��o da label lblPassosReceita
		JLabel lblPassosReceita = new JLabel("Passos:");
		lblPassosReceita.setBounds(40, 457, 46, 14);
		panelReceita.add(lblPassosReceita);

		// Declara��o da label lblTipoReceita
		JLabel lblTipoReceita = new JLabel("Tipo:");
		lblTipoReceita.setBounds(40, 422, 46, 14);
		panelReceita.add(lblTipoReceita);

		// Declara��o da comboBox comboBoxTipoReceita
		JComboBox comboBoxTipoReceita = new JComboBox();
		comboBoxTipoReceita.setBounds(108, 413, 190, 33);
		panelReceita.add(comboBoxTipoReceita);

		// Declara��o da button btnGuardaReceita
		JButton btnGuardaReceita = new JButton("Guardar");
		btnGuardaReceita.setBounds(645, 325, 108, 33);
		panelReceita.add(btnGuardaReceita);

		// Declara��o da button btnLimpaReceita
		JButton btnLimpaReceita = new JButton("Limpar");

		btnLimpaReceita.setBounds(645, 369, 108, 33);
		panelReceita.add(btnLimpaReceita);

		// Declara��o da button btnCancelaReceita
		JButton btnCancelaReceita = new JButton("Cancelar");
		btnCancelaReceita.setBounds(645, 413, 108, 33);
		panelReceita.add(btnCancelaReceita);

		// Declara��o do textField textFieldConfecaoReceita
		textFieldConfecaoReceita = new JTextField();
		textFieldConfecaoReceita.setColumns(10);
		textFieldConfecaoReceita.setBounds(403, 369, 196, 33);
		panelReceita.add(textFieldConfecaoReceita);

		// Declara��o da label lblNDosesReceita
		JLabel lblNDosesReceita = new JLabel("N\u00BA Doses:");
		lblNDosesReceita.setBounds(40, 334, 58, 14);
		panelReceita.add(lblNDosesReceita);

		// Declara��o da label lblConfecaoReceita
		JLabel lblConfecaoReceita = new JLabel("Confe\u00E7\u00E3o:");
		lblConfecaoReceita.setBounds(308, 378, 58, 14);
		panelReceita.add(lblConfecaoReceita);

		// Declara��o da label lblClassificaoReceita
		JLabel lblClassificaoReceita = new JLabel("Classifica\u00E7\u00E3o:");
		lblClassificaoReceita.setBounds(309, 422, 84, 14);
		panelReceita.add(lblClassificaoReceita);

		// Declara��o da comboBox comboBoxClassificacaoReceita
		JComboBox comboBoxClassificacaoReceita = new JComboBox();
		comboBoxClassificacaoReceita.addItem(1);
		comboBoxClassificacaoReceita.addItem(2);
		comboBoxClassificacaoReceita.addItem(3);
		comboBoxClassificacaoReceita.addItem(4);
		comboBoxClassificacaoReceita.addItem(5);
		comboBoxClassificacaoReceita.setBounds(403, 413, 196, 33);
		panelReceita.add(comboBoxClassificacaoReceita);

		// Declara��o do textField textFieldNDosesReceitas
		textFieldNDosesReceitas = new JTextField();
		textFieldNDosesReceitas.setColumns(10);
		textFieldNDosesReceitas.setBounds(108, 325, 491, 33);
		panelReceita.add(textFieldNDosesReceitas);
		DefaultComboBoxModel listComboBoxTipoReceita = new DefaultComboBoxModel();
		comboBoxTipoReceita.setModel(listComboBoxTipoReceita);

		// Declara��o da label labelUnidades
		JLabel labelUnidades = new JLabel("Unidade: ");
		labelUnidades.setBounds(321, 185, 72, 14);
		panelReceita.add(labelUnidades);

		// Declara��o da comboBoxUnidades
		JComboBox comboBoxUnidades = new JComboBox();
		comboBoxUnidades.addItem("Kg");
		comboBoxUnidades.addItem("L");
		comboBoxUnidades.addItem("ml");
		comboBoxUnidades.addItem("g");
		comboBoxUnidades.addItem("Unid");
		comboBoxUnidades.setBounds(384, 182, 72, 20);
		panelReceita.add(comboBoxUnidades);

		//Inicio da declara��o dos eventos
		
		// Declara��o do evento mouseClicked do button btnGuardaReceita
		// Fun��o do evento: Inserir dados na BD na tabela dos receita
		btnGuardaReceita.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					stmt = conn.createStatement();
					String nomeReceita = textFieldNomeReceita.getText();
					String nomeTipoReceita = comboBoxTipoReceita.getSelectedItem().toString();
					String passosReceita = textAreaPassosReceita.getText();
					int nDoses = Integer.parseInt(textFieldNDosesReceitas.getText());
					int tempoConfecao = Integer.parseInt(textFieldConfecaoReceita.getText());
					int tempoPreparacao = Integer.parseInt(textFieldDuracaoReceita.getText());
					int caloriasReceita = 0;
					int classificacao = Integer.parseInt(comboBoxClassificacaoReceita.getSelectedItem().toString());
					int teste = 2;

					result = stmt.executeQuery("SELECT nomeReceita from receita");
					int verificaNome = 0;
					while (result.next()) {
						if (!nomeReceita.equals(result.getString(1))) {
							verificaNome = 1;
						} else {
							int dialogButton = JOptionPane.YES_NO_OPTION;
							int dialogResult = JOptionPane.showConfirmDialog(null,
									"J� existe uma receita com o mesmo nome, deseja inserir uma receita com o mesmo nome?",
									"Aten��o", dialogButton);
							if (dialogResult == JOptionPane.YES_OPTION) {

								ResultSet nome = stmt.executeQuery("SELECT nomeReceita from receita");

								while (result.next()) {
									nomeReceita = textFieldNomeReceita.getText() + " (" + teste + ")";
									if (nomeReceita.equals(result.getString(1))) {
										teste++;
										nomeReceita = textFieldNomeReceita.getText() + " (" + teste + ")";
									} else {
										verificaNome = 1;
									}
								}
							} else {
								verificaNome = 0;
								break;
							}
						}
					}

					if (verificaNome == 1) {
						if (!nomeReceita.equals("") && nDoses >= 0 && !nomeTipoReceita.equals("")
								&& !passosReceita.equals("") && tempoConfecao >= 0 && tempoPreparacao >= 0
								&& caloriasReceita >= 0 && classificacao >= 0
								&& tableIngredientesEscolhidos.getRowCount() > 0) {

							for (int i = 0; i < tableIngredientesEscolhidos.getRowCount(); i++) {
								String nomeIngrediente = tableIngredientesEscolhidos.getModel().getValueAt(i, 0)
										.toString();
								int calorias = Integer
										.parseInt(tableIngredientesEscolhidos.getModel().getValueAt(i, 1).toString());
								int qtd = Integer
										.parseInt(tableIngredientesEscolhidos.getModel().getValueAt(i, 3).toString());
								String unidadades = tableIngredientesEscolhidos.getModel().getValueAt(i, 4).toString();
								if (unidadades.equals("g")) {
									caloriasReceita = caloriasReceita + ((calorias * qtd) / 100);
								} else if (unidadades.equals("Kg")) {
									caloriasReceita = caloriasReceita + (calorias * qtd);
								} else if (unidadades.equals("L")) {
									caloriasReceita = caloriasReceita + (calorias * qtd);
								} else if (unidadades.equals("ml")) {
									caloriasReceita = caloriasReceita + ((calorias * qtd) / 100);

								}
								else {
									caloriasReceita = caloriasReceita + (calorias * qtd);
								}
								stmt.executeUpdate(
										"insert into detalhesReceita (nomeReceita,nomeIngrediente,qtd, unidade)values('"
												+ nomeReceita + "', '" + nomeIngrediente + "','" + qtd + "','"
												+ unidadades + "')");
								System.out.println("entrou");
							}

							stmt.executeUpdate(
									"insert into receita (nomeReceita,nDoses,nomeTipoReceita,passosReceita,tempoConfecao,tempoPreparacao,caloriasReceita, classificacao)values('"
											+ nomeReceita + "', '" + nDoses + "','" + nomeTipoReceita + "', '"
											+ passosReceita + "', '" + tempoConfecao + "', '" + tempoPreparacao + "', '"
											+ caloriasReceita + "', '" + classificacao + "')");

							LimparTabelaReceitas();
							CarregarTabelaReceitas();
							textAreaPassosReceita.setText("");
							panelReceita.setVisible(false);
							panelMain.setVisible(true);
						}
					}
				} catch (Exception err) {
					System.out.println(err);
					JOptionPane.showMessageDialog(null, "Preencha os campos corretamente!", "Erro",
							JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});

		// Declara��o do evento mouseClicked do button btnLimpaReceita
		// Fun��o do evento: Limpa textfield e table do panelReceita
		btnLimpaReceita.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				LimparTabelaReceitas();
				textAreaPassosReceita.setText("");
			}
		});

		// Declara��o do evento mouseClicked do button btnRemoveIngrediente
		// Fun��o do evento: Tira os ingredientes da tableIngredientesEscolhidos
		btnRemoveIngrediente.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				DefaultTableModel tableIngredientesEscolhidosModel = (DefaultTableModel) tableIngredientesEscolhidos
						.getModel();

				tableIngredientesEscolhidosModel.removeRow(tableIngredientesEscolhidos.getSelectedRow());

			}
		});

		// Declara��o do evento mouseClicked do button btnAdicionaIngrediente
		// Fun��o do evento: Preenche tableIngredientesEscolhidos
		btnAdicionaIngrediente.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				try {
					DefaultTableModel tableIngredientesEscolhidosModel = (DefaultTableModel) tableIngredientesEscolhidos.getModel();

					int row = tableIngredientesExistentes.getSelectedRow();
					String col1 = tableIngredientesExistentes.getModel().getValueAt(row, 0).toString();
					String col2 = tableIngredientesExistentes.getModel().getValueAt(row, 1).toString();
					String col3 = tableIngredientesExistentes.getModel().getValueAt(row, 2).toString();
					int col4 = Integer.parseInt(textFieldQtd.getText());
					String col5 = comboBoxUnidades.getSelectedItem().toString();

					boolean verificaNome = false;

					for (int i = 0; i < tableIngredientesEscolhidos.getRowCount(); i++) {
						if (col1.equals(tableIngredientesEscolhidos.getModel().getValueAt(i, 0))) {
							verificaNome = true;
							break;
						}
					}

					if (!verificaNome)
						tableIngredientesEscolhidosModel.addRow(new Object[] { col1, col2, col3, col4, col5 });

				} catch (Exception err) {
				}
			}
		});

		// Declara��o do evento mouseClicked do button btnCancelaReceita
		// Fun��o do evento: Muda para o panelMain
		btnCancelaReceita.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				panelMain.setVisible(true);
				panelReceita.setVisible(false);
			}
		});

		// Meter os outros paineis escondidos no inicio da aplica��o
		panelReceita.setVisible(false);

		// Declara��o do evento mouseClicked do button buttonCancelarIngrediente
		// Fun��o do evento: Muda para o panelMain
		buttonCancelarIngrediente.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				panelMain.setVisible(true);
				panelIngredientes.setVisible(false);
			}
		});

		panelIngredientes.setVisible(false);

		// Inserir dados na BD na tabela dos ingrediente
		buttonGuardarIngredientes.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					stmt = conn.createStatement();
					String nome = textFieldNomeIngrediente.getText();
					int calorias = Integer.parseInt(textFieldCaloriasIngrediente.getText());
					String desc = textFieldDescricaoIngrediente.getText();

					result = stmt.executeQuery("SELECT nome from ingrediente");
					int verificaNome = 0;
					while (result.next()) {
						if (!nome.equals(result.getString(1))) {
							verificaNome = 1;
						} else {
							verificaNome = 0;
							break;
						}
					}

					if (verificaNome == 1) {
						if (!nome.equals("") && calorias >= 0) {
							stmt.executeUpdate("insert into ingrediente (nome,calorias,desc)values('" + nome + "','"
									+ calorias + "','" + desc + "')");
							LimparTabelaIngredientes();
							CarregaTabelaIngredienteExistente();
						}else {
							JOptionPane.showMessageDialog(null, "Preencha os campos corretamente!", "Erro",
									JOptionPane.INFORMATION_MESSAGE);
						}
					} else {
						JOptionPane.showMessageDialog(null, "Nome do ingrediente j� inserido!", "Erro",
								JOptionPane.INFORMATION_MESSAGE);
					}

				} catch (Exception err) {

				}
			}
		});

		// Eliminar dados na tabela dos IngredienteExistentes
		buttonEliminarIngrediente.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				try {
					DefaultTableModel tableIngredientesExistenteModel = (DefaultTableModel) tableIngredientesExistente
							.getModel();
					int row = tableIngredientesExistente.getSelectedRow();

					if (!(row == -1)) {

						int dialogButton = JOptionPane.YES_NO_OPTION;
						int dialogResult = JOptionPane.showConfirmDialog(null, "Deseja mesmo eliminar o ingrediente?",
								"Aten��o", dialogButton);
						if (dialogResult == JOptionPane.YES_OPTION) {

							String value = tableIngredientesExistente.getModel().getValueAt(row, 0).toString();

							stmt = conn.createStatement();
							stmt.executeUpdate("Delete from ingrediente where nome = '" + value + "'");

							tableIngredientesExistenteModel.removeRow(tableIngredientesExistente.getSelectedRow());
							CarregaTabelaIngredienteExistente();

						}
					} else {
						JOptionPane.showMessageDialog(null, "Selecione o ingrediente que deseja eliminar", "Erro",
								JOptionPane.INFORMATION_MESSAGE);
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.out.println("Erro, necessita de selecionar uma linha da tabela");
				}
			}
		});

		// Limpar textFields do painel Ingredientes
		buttonLimparIngrediente.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});

		// Declara��o do evento mouseClicked do button buttonPesquisa
		// Fun��o do evento: Muda para o painel de pesquisas
		buttonPesquisa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				panelMain.setVisible(false);
				panelPesquisa.setVisible(true);
				CarregarTabelaReceitas();
			}
		});

		// Declara��o do evento mouseClicked do button btnEditReceita
		// Fun��o do evento: Preenche os campos com os mesmos campos da receita para
		// poderem ser editados
		btnEditReceita.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (tableReceitas.getSelectionModel().isSelectionEmpty()) {

				} else {
					panelMain.setVisible(false);
					panelEditReceita.setVisible(true);
					CarregaTabelaIngredienteExistente();
					CarregarTabelaEditIngredientesEscolhidos();

					int row = tableReceitas.getSelectedRow();
					textFieldEditNomeReceita.setText(tableReceitas.getModel().getValueAt(row, 0).toString());
					textFieldEditNDosesReceita.setText(tableReceitas.getModel().getValueAt(row, 2).toString());
					textFieldEditDuracaoReceita.setText(tableReceitas.getModel().getValueAt(row, 3).toString());
					textFieldEditConfecaoReceita.setText(tableReceitas.getModel().getValueAt(row, 4).toString());
					comboBoxEditClassificacaoReceeita.setSelectedItem(tableReceitas.getModel().getValueAt(row, 5));
					comboBoxEditTipoReceita.setSelectedItem(tableReceitas.getModel().getValueAt(row, 6));
					textAreaEditPassosReceita.setText(textAreaPassosReceitasMain.getText());

					try {
						stmt = conn.createStatement();
						result = stmt.executeQuery("Select idReceita from receita where nomeReceita = '"
								+ tableReceitas.getModel().getValueAt(row, 0).toString() + "'");
						lblEditIDReceita.setText(result.getString("idReceita"));

					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						System.out.println("Erro, necessita de selecionar uma linha da tabela");
					}

				}
			}
		});

		// Eliminar dados na tabela das Receitas
		btnDeleteReceita.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					DefaultTableModel tableReceitasModel = (DefaultTableModel) tableReceitas.getModel();

					int row = tableReceitas.getSelectedRow();

					if (!(row == -1)) {

						int dialogButton = JOptionPane.YES_NO_OPTION;
						int dialogResult = JOptionPane.showConfirmDialog(null, "Deseja mesmo eliminar a receita?",
								"Aten��o", dialogButton);
						if (dialogResult == JOptionPane.YES_OPTION) {

							String value = tableReceitas.getModel().getValueAt(row, 0).toString();

							stmt = conn.createStatement();
							stmt.executeUpdate("Delete from receita where nomeReceita = '" + value + "'");
							stmt.executeUpdate("Delete from detalhesReceita where nomeReceita = '" + value + "'");

							DefaultTableModel tableIngredientesModel = (DefaultTableModel) tableIngredientes.getModel();
							
							int rowCount = tableIngredientesModel.getRowCount();
							for (int i = rowCount - 1; i >= 0; i--) {
								tableIngredientesModel.removeRow(i);
							}
						   

							textAreaPassosReceitasMain.setText("");
							tableReceitasModel.removeRow(tableReceitas.getSelectedRow());
							CarregarTabelaReceitas();
						}
					} else {
						JOptionPane.showMessageDialog(null, "Selecione a receita que deseja eliminar", "Erro",
								JOptionPane.INFORMATION_MESSAGE);
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.out.println("Erro, necessita de selecionar uma linha da tabela");
				}
			}
		});

		// Declara��o do evento mouseClicked do button buttonAddReceita
		// Fun��o do evento: Muda para o panelReceita
		buttonAddReceita.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				panelMain.setVisible(false);
				panelReceita.setVisible(true);
				CarregaTabelaIngredienteExistente();
				LimparTabelaReceitas();
			}
		});

		// Declara��o do evento mouseClicked do button btnIngredientes
		// Fun��o do evento: Muda para o panelIngredientes
		btnIngredientes.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				panelMain.setVisible(false);
				panelIngredientes.setVisible(true);
				textFieldNomeIngrediente.setText("");
				textFieldCaloriasIngrediente.setText("");
				textFieldDescricaoIngrediente.setText("");
				CarregaTabelaIngredienteExistente();
			}
		});

		// Declara��o do evento mouseClicked do table tableReceitas
		// Fun��o do evento: Adiciona texto � lista passosReceita
		tableReceitas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				DefaultTableModel tableIngredientesModel = (DefaultTableModel) tableIngredientes.getModel();
				tableIngredientesModel.getDataVector().removeAllElements();
				int row = tableReceitas.getSelectedRow();
				String col1 = tableReceitas.getModel().getValueAt(row, 0).toString();

				String addPassos;
				String addNomeIngrediente;
				int addQtd;
				String addUnidade;
				try {
					stmt = conn.createStatement();
					result = stmt.executeQuery("SELECT passosReceita FROM receita WHERE nomeReceita = '" + col1 + "'");
					while (result.next()) {
						addPassos = result.getString("passosReceita");
						textAreaPassosReceitasMain.setText(addPassos);
					}

					result = stmt.executeQuery(
							"SELECT nomeIngrediente,qtd,unidade FROM detalhesReceita WHERE nomeReceita = '" + col1
									+ "'");
					while (result.next()) {
						addNomeIngrediente = result.getString("nomeIngrediente");
						addQtd = Integer.parseInt(result.getString("qtd"));
						addUnidade = result.getString("unidade");
						tableIngredientesModel.addRow(new Object[] { addNomeIngrediente, addQtd, addUnidade });
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		// Declara��o do evento mouseClicked do table tableReceitaPesquisa
		// Fun��o do evento: Adiciona texto � lista passosReceitaPesquisa e � lista dos ingredientes
		tableReceitaPesquisa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				DefaultTableModel tableIngredientesPesquisaModel = (DefaultTableModel) tableIngredientesPesquisa.getModel();
				tableIngredientesPesquisaModel.getDataVector().removeAllElements();
				int row = tableReceitaPesquisa.getSelectedRow();
				String col1 = tableReceitaPesquisa.getModel().getValueAt(row, 0).toString();
				
				String addPassos;
				String addNomeIngrediente;
				int addQtd;
				String addUnidade;
				
				try {
					stmt = conn.createStatement();
					result = stmt.executeQuery("SELECT passosReceita FROM receita WHERE nomeReceita = '" + col1 + "'");
					while (result.next()) {
						addPassos = result.getString("passosReceita");
						textAreaPassosPesquisa.setText(addPassos);
					}

					result = stmt.executeQuery(
							"SELECT nomeIngrediente,qtd,unidade FROM detalhesReceita WHERE nomeReceita = '" + col1
									+ "'");
					while (result.next()) {
						addNomeIngrediente = result.getString("nomeIngrediente");
						addQtd = Integer.parseInt(result.getString("qtd"));
						addUnidade = result.getString("unidade");
						tableIngredientesPesquisaModel.addRow(new Object[] { addNomeIngrediente, addQtd, addUnidade });
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});

		// Declara��o do evento mouseClicked do button btnEditRemoveIngrediente
		// Fun��o do evento: Tira os ingredientes da tableEditIngredientesEscolhidos
		btnEditRemoveIngrediente.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				DefaultTableModel tableEditIngredientesEscolhidosModel = (DefaultTableModel) tableEditIngredientesEscolhidos
						.getModel();

				tableEditIngredientesEscolhidosModel.removeRow(tableEditIngredientesEscolhidos.getSelectedRow());
			}
		});

		// Declara��o do evento mouseClicked do button btnEditAdicionaIngrediente
		// Fun��o do evento: Serve para adicionar um ingrediente da tabela dos
		// ingredientes existentes � tabela dos ingredientes escolhidos
		btnEditAdicionaIngrediente.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				try {
					DefaultTableModel tableEditIngredientesEscolhidosModel = (DefaultTableModel) tableEditIngredientesEscolhidos.getModel();

					int row = tableEditIngredientesExistentes.getSelectedRow();
					String col1 = tableEditIngredientesExistentes.getModel().getValueAt(row, 0).toString();
					String col2 = tableEditIngredientesExistentes.getModel().getValueAt(row, 1).toString();
					String col3 = tableEditIngredientesExistentes.getModel().getValueAt(row, 2).toString();
					int col4 = Integer.parseInt(textFieldEditQtd.getText());
					String col5 = comboBoxEditUnidade.getSelectedItem().toString();

					boolean verificaNome = false;

					for (int i = 0; i < tableEditIngredientesEscolhidos.getRowCount(); i++) {
						if (col1.equals(tableEditIngredientesEscolhidos.getModel().getValueAt(i, 0))) {
							verificaNome = true;
							break;
						}
					}

					if (!verificaNome)
						tableEditIngredientesEscolhidosModel.addRow(new Object[] { col1, col2, col3, col4, col5 });

				} catch (Exception err) {
				}
			}
		});

		// Declara��o do evento mouseClicked do button btnEditCancelaReceita
		// Fun��o do evento: Volta para o menu receita
		btnEditCancelaReceita.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				panelEditReceita.setVisible(false);
				panelMain.setVisible(true);
			}
		});
		panelEditReceita.setVisible(false);

		// Declara��o do evento mouseClicked do button btnPesquisar
		// Fun��o do evento: Pesquisa dados na BD
		btnPesquisar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				DefaultTableModel listTableReceitasPesquisa = new DefaultTableModel();

				listTableReceitasPesquisa.addColumn("Nome");
				listTableReceitasPesquisa.addColumn("Calorias");
				listTableReceitasPesquisa.addColumn("N� Doses");
				listTableReceitasPesquisa.addColumn("Prepara��o (m)");
				listTableReceitasPesquisa.addColumn("Confe��o (m)");
				listTableReceitasPesquisa.addColumn("Classifica��o");
				listTableReceitasPesquisa.addColumn("Tipo");
				tableReceitaPesquisa.setDefaultEditor(Object.class, null);

				try {
					stmt = conn.createStatement();

					String opcaoCalorias, opcaoNDoses, calorias, nDoses, classificacao, tipo, nome;
					int addCalorias, addNDoses, addTempoPreparacao, addTempoConfecao, addClassificacao;
					String addNomeReceita, addTipoReceita;

					opcaoCalorias = comboBoxCaloriasPesquisa.getSelectedItem().toString();
					opcaoNDoses = comboBoxNDosesPesquisa.getSelectedItem().toString();
					classificacao = comboBoxClassificacaoPesquisa.getSelectedItem().toString();
					tipo = comboBoxTipoPesquisa.getSelectedItem().toString();
					calorias = textFieldCaloriasPesquisa.getText();
					nDoses = textFieldNDosesPesquisa.getText();
					nome = textFieldNomePesquisa.getText();

					// Se tudo for vazio menos o nome
					if (!nome.isEmpty() && nDoses.isEmpty() && calorias.isEmpty() && tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						result = stmt.executeQuery("SELECT * FROM receita Where nomeReceita LIKE '" + nome + "%"
								+ "' Order by nomeReceita");
						while (result.next()) {
							addNomeReceita = result.getString("nomeReceita");
							addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
							addNDoses = Integer.parseInt(result.getString("nDoses"));
							addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
							addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
							addClassificacao = Integer.parseInt(result.getString("classificacao"));
							addTipoReceita = result.getString("nomeTipoReceita");

							listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
									addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
						}
					}

					// Se tudo for vazio menos o tipo
					if (nome.isEmpty() && nDoses.isEmpty() && calorias.isEmpty() && !tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						result = stmt.executeQuery(
								"SELECT * FROM receita Where nomeTipoReceita LIKE '" + tipo + "' Order by nomeReceita");
						while (result.next()) {
							addNomeReceita = result.getString("nomeReceita");
							addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
							addNDoses = Integer.parseInt(result.getString("nDoses"));
							addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
							addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
							addClassificacao = Integer.parseInt(result.getString("classificacao"));
							addTipoReceita = result.getString("nomeTipoReceita");

							listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
									addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
						}
					}

					// Se tudo for vazio menos a classificacao
					if (nome.isEmpty() && nDoses.isEmpty() && calorias.isEmpty() && tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						result = stmt.executeQuery("SELECT * FROM receita Where classificacao LIKE '" + classificacao
								+ "' Order by nomeReceita");
						while (result.next()) {
							addNomeReceita = result.getString("nomeReceita");
							addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
							addNDoses = Integer.parseInt(result.getString("nDoses"));
							addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
							addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
							addClassificacao = Integer.parseInt(result.getString("classificacao"));
							addTipoReceita = result.getString("nomeTipoReceita");

							listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
									addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
						}
					}

					// Se tudo for vazio menos as calorias
					if (nome.isEmpty() && nDoses.isEmpty() && !calorias.isEmpty() && tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						if (opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita Where caloriasReceita  >= '" + calorias
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita Where caloriasReceita  <= '" + calorias
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as doses
					if (nome.isEmpty() && !nDoses.isEmpty() && calorias.isEmpty() && tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=")) {
							result = stmt.executeQuery(
									"SELECT * FROM receita Where nDoses  >= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery(
									"SELECT * FROM receita Where nDoses  <= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos o nome e as doses
					if (!nome.isEmpty() && !nDoses.isEmpty() && calorias.isEmpty() && tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE '" + nome + "%"
									+ "' AND nDoses  >= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE '" + nome + "%"
									+ "' AND nDoses  <= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos o nome e as calorias
					if (!nome.isEmpty() && nDoses.isEmpty() && !calorias.isEmpty() && tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						if (opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE '" + nome + "%"
									+ "' AND caloriasReceita  >= '" + calorias + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE '" + nome + "%"
									+ "' AND caloriasReceita  <= '" + calorias + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos o nome e o tipo
					if (!nome.isEmpty() && nDoses.isEmpty() && calorias.isEmpty() && !tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						result = stmt.executeQuery("SELECT * FROM receita Where nomeReceita LIKE '" + nome + "%"
								+ "' AND nomeTipoReceita LIKE '" + tipo + "' Order by nomeReceita");
						while (result.next()) {
							addNomeReceita = result.getString("nomeReceita");
							addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
							addNDoses = Integer.parseInt(result.getString("nDoses"));
							addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
							addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
							addClassificacao = Integer.parseInt(result.getString("classificacao"));
							addTipoReceita = result.getString("nomeTipoReceita");

							listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
									addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
						}
					}

					// Se tudo for vazio menos o nome e a classifica��o
					if (!nome.isEmpty() && nDoses.isEmpty() && calorias.isEmpty() && tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						result = stmt.executeQuery("SELECT * FROM receita Where nomeReceita LIKE '" + nome + "%"
								+ "' AND classificacao LIKE '" + classificacao + "' Order by nomeReceita");
						while (result.next()) {
							addNomeReceita = result.getString("nomeReceita");
							addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
							addNDoses = Integer.parseInt(result.getString("nDoses"));
							addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
							addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
							addClassificacao = Integer.parseInt(result.getString("classificacao"));
							addTipoReceita = result.getString("nomeTipoReceita");

							listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
									addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
						}
					}

					// Se tudo for vazio menos as doses e o tipo
					if (nome.isEmpty() && !nDoses.isEmpty() && calorias.isEmpty() && !tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE '" + tipo
									+ "' AND nDoses  >= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE '" + tipo
									+ "' AND nDoses  <= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as doses e o classificao
					if (nome.isEmpty() && !nDoses.isEmpty() && calorias.isEmpty() && tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE classificacao LIKE '"
									+ classificacao + "' AND nDoses  >= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE classificacao LIKE '"
									+ classificacao + "' AND nDoses  <= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as doses e as calorias
					if (nome.isEmpty() && !nDoses.isEmpty() && !calorias.isEmpty() && tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE caloriasReceita >= '" + calorias
									+ "' AND nDoses  >= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals(">=") && opcaoCalorias.equals("<=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE caloriasReceita <= '" + calorias
									+ "' AND nDoses  >= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals("<=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE caloriasReceita >= '" + calorias
									+ "' AND nDoses  <= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE caloriasReceita <= '" + calorias
									+ "' AND nDoses  <= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as calorias e o tipo
					if (nome.isEmpty() && nDoses.isEmpty() && !calorias.isEmpty() && !tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						if (opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE '" + tipo
									+ "' AND caloriasReceita  >= '" + calorias + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE '" + tipo
									+ "' AND caloriasReceita  <= '" + calorias + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as calorias e a classificacao
					if (nome.isEmpty() && nDoses.isEmpty() && !calorias.isEmpty() && tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						if (opcaoCalorias.equals(">=")) {
							result = stmt
									.executeQuery("SELECT * FROM receita WHERE classificacao LIKE '" + classificacao
											+ "' AND caloriasReceita  >= '" + calorias + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt
									.executeQuery("SELECT * FROM receita WHERE classificacao LIKE '" + classificacao
											+ "' AND caloriasReceita  <= '" + calorias + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos o tipo e a classificacao
					if (nome.isEmpty() && nDoses.isEmpty() && calorias.isEmpty() && !tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						result = stmt.executeQuery("SELECT * FROM receita Where nomeTipoReceita LIKE '" + tipo
								+ "' AND classificacao LIKE '" + classificacao + "' Order by nomeReceita");
						while (result.next()) {
							addNomeReceita = result.getString("nomeReceita");
							addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
							addNDoses = Integer.parseInt(result.getString("nDoses"));
							addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
							addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
							addClassificacao = Integer.parseInt(result.getString("classificacao"));
							addTipoReceita = result.getString("nomeTipoReceita");

							listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
									addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
						}
					}

					// Se tudo for vazio menos as doses, as calorias e o nome
					if (!nome.isEmpty() && !nDoses.isEmpty() && !calorias.isEmpty() && tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND caloriasReceita >= '" + calorias + "' AND nDoses  >= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals(">=") && opcaoCalorias.equals("<=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND caloriasReceita <= '" + calorias + "' AND nDoses  >= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals("<=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND caloriasReceita >= '" + calorias + "' AND nDoses  <= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND caloriasReceita <= '" + calorias + "' AND nDoses  <= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as doses, o nome e o tipo
					if (!nome.isEmpty() && !nDoses.isEmpty() && calorias.isEmpty() && !tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND nomeTipoReceita LIKE '" + tipo + "' AND nDoses  >= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND nomeTipoReceita LIKE '" + tipo + "' AND nDoses  <= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as calorias, o nome e o tipo
					if (!nome.isEmpty() && nDoses.isEmpty() && !calorias.isEmpty() && !tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND nomeTipoReceita LIKE '" + tipo + "' AND caloriasReceita  >= '" + calorias
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND nomeTipoReceita LIKE '" + tipo + "' AND caloriasReceita  <= '" + calorias
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as calorias, o nome e o classificao
					if (!nome.isEmpty() && nDoses.isEmpty() && !calorias.isEmpty() && tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao LIKE '" + classificacao + "' AND caloriasReceita  >= '"
									+ calorias + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao LIKE '" + classificacao + "' AND caloriasReceita  <= '"
									+ calorias + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as doses, o nome e o classificacao
					if (!nome.isEmpty() && !nDoses.isEmpty() && calorias.isEmpty() && tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao LIKE '" + classificacao + "' AND nDoses  >= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao LIKE '" + classificacao + "' AND nDoses  <= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as doses, as calorias e o tipo
					if (nome.isEmpty() && !nDoses.isEmpty() && !calorias.isEmpty() && !tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE  '" + tipo
									+ "' AND caloriasReceita >= '" + calorias + "' AND nDoses  >= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals(">=") && opcaoCalorias.equals("<=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE  '" + tipo
									+ "' AND caloriasReceita <= '" + calorias + "' AND nDoses  >= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals("<=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE  '" + tipo
									+ "' AND caloriasReceita >= '" + calorias + "' AND nDoses  <= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE  '" + tipo
									+ "' AND caloriasReceita <= '" + calorias + "' AND nDoses  <= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as doses, as calorias e a classificacao
					if (nome.isEmpty() && !nDoses.isEmpty() && !calorias.isEmpty() && tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE classificacao LIKE  '"
									+ classificacao + "' AND caloriasReceita >= '" + calorias + "' AND nDoses  >= '"
									+ nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals(">=") && opcaoCalorias.equals("<=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE classificacao LIKE  '"
									+ classificacao + "' AND caloriasReceita <= '" + calorias + "' AND nDoses  >= '"
									+ nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals("<=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE classificacao LIKE  '"
									+ classificacao + "' AND caloriasReceita >= '" + calorias + "' AND nDoses  <= '"
									+ nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE classificacao LIKE  '"
									+ classificacao + "' AND caloriasReceita <= '" + calorias + "' AND nDoses  <= '"
									+ nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as doses, o tipo e a classificacao
					if (nome.isEmpty() && !nDoses.isEmpty() && calorias.isEmpty() && !tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE  '" + tipo
									+ "' AND classificacao LIKE '" + classificacao + "' AND nDoses  >= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE  '" + tipo
									+ "' AND classificacao LIKE '" + classificacao + "' AND nDoses  <= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as calorias, o tipo e a classificacao
					if (nome.isEmpty() && nDoses.isEmpty() && !calorias.isEmpty() && !tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE  '" + tipo
									+ "' AND classificacao LIKE '" + classificacao + "' AND caloriasReceita  >= '"
									+ calorias + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE  '" + tipo
									+ "' AND classificacao LIKE '" + classificacao + "' AND caloriasReceita  <= '"
									+ calorias + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as nome, o tipo e a classificacao
					if (!nome.isEmpty() && nDoses.isEmpty() && calorias.isEmpty() && !tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
								+ "' AND nomeTipoReceita LIKE  '" + tipo + "' AND classificacao LIKE '" + classificacao
								+ "' Order by nomeReceita");
						while (result.next()) {
							addNomeReceita = result.getString("nomeReceita");
							addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
							addNDoses = Integer.parseInt(result.getString("nDoses"));
							addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
							addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
							addClassificacao = Integer.parseInt(result.getString("classificacao"));
							addTipoReceita = result.getString("nomeTipoReceita");

							listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
									addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
						}
					}

					// Se tudo for vazio menos as doses, as calorias, o tipo e o nome
					if (!nome.isEmpty() && !nDoses.isEmpty() && !calorias.isEmpty() && !tipo.equals("Todos")
							&& classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND nomeTipoReceita LIKE '" + tipo + "' AND caloriasReceita >= '" + calorias
									+ "' AND nDoses  >= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals(">=") && opcaoCalorias.equals("<=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND nomeTipoReceita  '" + tipo + "' AND caloriasReceita <= '" + calorias
									+ "' AND nDoses  >= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals("<=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND nomeTipoReceita LIKE '" + tipo + "' AND caloriasReceita >= '" + calorias
									+ "' AND nDoses  <= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND nomeTipoReceita LIKE '" + tipo + "' AND caloriasReceita <= '" + calorias
									+ "' AND nDoses  <= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as doses, as calorias, a classificacao e o nome
					if (!nome.isEmpty() && !nDoses.isEmpty() && !calorias.isEmpty() && tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao LIKE '" + classificacao + "' AND caloriasReceita >= '"
									+ calorias + "' AND nDoses  >= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals(">=") && opcaoCalorias.equals("<=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao  '" + classificacao + "' AND caloriasReceita <= '" + calorias
									+ "' AND nDoses  >= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals("<=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao LIKE '" + classificacao + "' AND caloriasReceita >= '"
									+ calorias + "' AND nDoses  <= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao LIKE '" + classificacao + "' AND caloriasReceita <= '"
									+ calorias + "' AND nDoses  <= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos o nome, a classificacao, o tipo e as doses
					if (!nome.isEmpty() && !nDoses.isEmpty() && calorias.isEmpty() && !tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao LIKE '" + classificacao + "' AND nomeTipoReceita LIKE '"
									+ tipo + "' AND nDoses  >= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao LIKE '" + classificacao + "' AND nomeTipoReceita LIKE '"
									+ tipo + "' AND nDoses  <= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos o nome, a classificacao, o tipo e as calorias
					if (!nome.isEmpty() && nDoses.isEmpty() && !calorias.isEmpty() && !tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao LIKE '" + classificacao + "' AND nomeTipoReceita LIKE '"
									+ tipo + "' AND caloriasReceita  >= '" + calorias + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao LIKE '" + classificacao + "' AND nomeTipoReceita LIKE '"
									+ tipo + "' AND caloriasReceita  <= '" + calorias + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}

					// Se tudo for vazio menos as doses, as calorias, a classificacao e o tipo
					if (nome.isEmpty() && !nDoses.isEmpty() && !calorias.isEmpty() && !tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE  '" + tipo
									+ "' AND classificacao LIKE '" + classificacao + "' AND caloriasReceita >= '"
									+ calorias + "' AND nDoses  >= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals(">=") && opcaoCalorias.equals("<=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE  '" + tipo
									+ "' AND classificacao LIKE '" + classificacao + "' AND caloriasReceita <= '"
									+ calorias + "' AND nDoses  >= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals("<=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE  '" + tipo
									+ "' AND classificacao LIKE '" + classificacao + "' AND caloriasReceita >= '"
									+ calorias + "' AND nDoses  <= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeTipoReceita LIKE  '" + tipo
									+ "' AND classificacao LIKE '" + classificacao + "' AND caloriasReceita <= '"
									+ calorias + "' AND nDoses  <= '" + nDoses + "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}
				

					// Se tudo for preenchido
					if (!nome.isEmpty() && !nDoses.isEmpty() && !calorias.isEmpty() && !tipo.equals("Todos")
							&& !classificacao.equals("Todos")) {
						if (opcaoNDoses.equals(">=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao LIKE '" + classificacao + "' AND nomeTipoReceita LIKE '"
									+ tipo + "' AND caloriasReceita >= '" + calorias + "' AND nDoses  >= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals(">=") && opcaoCalorias.equals("<=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao  '" + classificacao + "' AND nomeTipoReceita LIKE '" + tipo
									+ "' AND caloriasReceita <= '" + calorias + "' AND nDoses  >= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else if (opcaoNDoses.equals("<=") && opcaoCalorias.equals(">=")) {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao LIKE '" + classificacao + "' AND nomeTipoReceita LIKE '"
									+ tipo + "' AND caloriasReceita >= '" + calorias + "' AND nDoses  <= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						} else {
							result = stmt.executeQuery("SELECT * FROM receita WHERE nomeReceita LIKE  '" + nome + "%"
									+ "' AND classificacao LIKE '" + classificacao + "' AND nomeTipoReceita LIKE '"
									+ tipo + "' AND caloriasReceita <= '" + calorias + "' AND nDoses  <= '" + nDoses
									+ "' Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
						}
					}
					
					// Se tudo for vazio
					if (nome.isEmpty() && nDoses.isEmpty() && calorias.isEmpty() && tipo.equals("Todos") && classificacao.equals("Todos")) {
							result = stmt.executeQuery("SELECT * FROM receita Order by nomeReceita");
							while (result.next()) {
								addNomeReceita = result.getString("nomeReceita");
								addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
								addNDoses = Integer.parseInt(result.getString("nDoses"));
								addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
								addTempoConfecao = Integer.parseInt(result.getString("tempoConfecao"));
								addClassificacao = Integer.parseInt(result.getString("classificacao"));
								addTipoReceita = result.getString("nomeTipoReceita");

								listTableReceitasPesquisa.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses,
										addTempoPreparacao, addTempoConfecao, addClassificacao, addTipoReceita });
							}
					}

				} catch (Exception err) {
					System.out.println(err);
				}
				tableReceitaPesquisa.setModel(listTableReceitasPesquisa);
			}
		});

		// Declara��o do evento mouseClicked do button btnEditGuardaReceita
		// Fun��o do evento: Editar dados na BD na tabela dos receita
		btnEditGuardaReceita.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					stmt = conn.createStatement();
					String nomeEditReceita = textFieldEditNomeReceita.getText();

					

					String nomeEditTipoReceita = comboBoxEditTipoReceita.getSelectedItem().toString();
					String passosEditReceita = textAreaEditPassosReceita.getText();
					int nDosesEditReceita = Integer.parseInt(textFieldEditNDosesReceita.getText());
					int tempoConfecaoEditReceita = Integer.parseInt(textFieldEditConfecaoReceita.getText());
					int tempoPreparacaoEditReceita = Integer.parseInt(textFieldEditDuracaoReceita.getText());
					int caloriasReceitaEditReceita = 0;
					int classificacaoEditReceita = Integer.parseInt(comboBoxEditClassificacaoReceeita.getSelectedItem().toString());
					String id = lblEditIDReceita.getText();
					int caloriasReceita = 0;

					result = stmt.executeQuery("SELECT nomeReceita from receita");
					int verificaNome = 0;
					while (result.next()) {
						if (!nomeEditReceita.equals(result.getString(1))) {
							verificaNome = 1;
						} else {
							verificaNome = 0;
							int dialogButton = JOptionPane.YES_NO_OPTION;
							int dialogResult = JOptionPane.showConfirmDialog(null,
									"J� existe uma receita com o mesmo nome, deseja atualizar o nome da receita?",
									"Aten��o", dialogButton);
							if (dialogResult == JOptionPane.YES_OPTION) {
								verificaNome = 1;
							}else {
								verificaNome = 0;
								break;
							}
						}
					}

					if (verificaNome == 1) {

						stmt.executeUpdate("delete from detalhesReceita where nomeReceita = '" + nomeEditReceita + "'");
						
						if (!nomeEditReceita.equals("") && nDosesEditReceita >= 0 && !nomeEditTipoReceita.equals("")
								&& !passosEditReceita.equals("") && tempoConfecaoEditReceita >= 0
								&& tempoPreparacaoEditReceita >= 0 && caloriasReceitaEditReceita >= 0
								&& classificacaoEditReceita >= 0 && tableEditIngredientesEscolhidos.getRowCount() > 0) {

							for (int i = 0; i < tableEditIngredientesEscolhidos.getRowCount(); i++) {
								String nomeIngrediente = tableEditIngredientesEscolhidos.getModel().getValueAt(i, 0)
										.toString();
								int calorias = Integer.parseInt(
										tableEditIngredientesEscolhidos.getModel().getValueAt(i, 1).toString());
								int qtd = Integer.parseInt(
										tableEditIngredientesEscolhidos.getModel().getValueAt(i, 3).toString());
								String unidadades = tableEditIngredientesEscolhidos.getModel().getValueAt(i, 4)
										.toString();
								if (unidadades.equals("g")) {
									caloriasReceitaEditReceita = caloriasReceitaEditReceita + ((calorias * qtd) / 100);
								} else if (unidadades.equals("Kg")) {
									caloriasReceitaEditReceita = caloriasReceitaEditReceita + (calorias * qtd);
								} else if (unidadades.equals("L")) {
									caloriasReceitaEditReceita = caloriasReceitaEditReceita + (calorias * qtd);
								} else if (unidadades.equals("ml")) {
									caloriasReceitaEditReceita = caloriasReceitaEditReceita + ((calorias * qtd) / 100);

								}else {
									caloriasReceitaEditReceita = caloriasReceitaEditReceita + (calorias * qtd);
								}
								stmt.executeUpdate(
										"insert into detalhesReceita (nomeReceita,nomeIngrediente,qtd, unidade)values('"
												+ nomeEditReceita + "', '" + nomeIngrediente + "','" + qtd + "','"
												+ unidadades + "')");
							}

							stmt.executeUpdate("UPDATE receita SET nomeReceita = '" + nomeEditReceita + "', nDoses = '"
									+ nDosesEditReceita + "', tempoPreparacao = '" + tempoPreparacaoEditReceita
									+ "',  tempoConfecao = '" + tempoConfecaoEditReceita + "', classificacao = '"
									+ classificacaoEditReceita + "', nomeTipoReceita = '" + nomeEditTipoReceita
									+ "', passosReceita = '" + passosEditReceita + "', caloriasReceita = '"
									+ caloriasReceitaEditReceita + "' WHERE idReceita = '" + id + "'");
						}else {
							JOptionPane.showMessageDialog(null, "Preencha os campos corretamente!", "Erro",
									JOptionPane.INFORMATION_MESSAGE);
						}
					}
					
					LimparEditTabelaReceitas();
					textAreaEditPassosReceita.setText("");
					CarregarTabelaReceitas();
					panelEditReceita.setVisible(false);
					panelMain.setVisible(true);

				} catch (Exception err) {
					System.out.println(err);
				
				}
			}
		});

		// Declara��o do evento mouseClicked do button buttonPesquisa
		// Fun��o do evento: Muda para o painel main
		buttonCancelarPesquisa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				panelPesquisa.setVisible(false);
				panelMain.setVisible(true);
				textFieldNomePesquisa.setText("");
				textFieldNDosesPesquisa.setText("");
				textFieldCaloriasPesquisa.setText("");
				textAreaPassosPesquisa.setText("");
				DefaultTableModel tableIngredientesPesquisaModel = (DefaultTableModel) tableIngredientesPesquisa.getModel();
				int rowCount = tableIngredientesPesquisaModel.getRowCount();
				for (int i = rowCount - 1; i >= 0; i--) {
					tableIngredientesPesquisaModel.removeRow(i);
				}
				
			}
		});
		panelPesquisa.setVisible(false);

		// Preencher a listsComboBoxTipoReceita com os dados da BD
		try {
			// assume that all objects were all properly defined
			String addNome;
			stmt = conn.createStatement();
			result = stmt.executeQuery("SELECT * FROM tipoReceita ORDER BY nome ASC");
			while (result.next()) {
				addNome = result.getString("nome");
				listComboBoxTipoReceita.addElement(addNome);
			}
		} catch (Exception err) {
			System.out.println(err);
		}

		// Preencher a listComboBoxEditTipoReceita com os dados da BD
		try {
			// assume that all objects were all properly defined
			String addNome;
			stmt = conn.createStatement();
			result = stmt.executeQuery("SELECT * FROM tipoReceita ORDER BY nome ASC");
			while (result.next()) {
				addNome = result.getString("nome");
				listComboBoxEditTipoReceita.addElement(addNome);
			}
		} catch (Exception err) {
			System.out.println(err);
		}

		// Preencher a listComboBoxTipoPesquisa com os dados da BD
		try {
			// assume that all objects were all properly defined
			String addNome;
			stmt = conn.createStatement();
			result = stmt.executeQuery("SELECT * FROM tipoReceita ORDER BY nome ASC");
			while (result.next()) {
				addNome = result.getString("nome");
				listComboBoxTipoPesquisa.addElement(addNome);
			}
			listComboBoxTipoPesquisa.addElement("Todos");
		} catch (Exception err) {
			System.out.println(err);
		}

		// Carrega a tabela das Receitas no menu principal quando o programa abre
		CarregarTabelaReceitas();

	}

	
	   /** M�todo para carregar os ingredientes existentes na base de dados para a tabela dos ingredientes existentes  */
	
	// Preencher a tableIngredientesExistente com os dados da BD
	public void CarregaTabelaIngredienteExistente() {
		DefaultTableModel lisTableIngredientesExistente = new DefaultTableModel();
		lisTableIngredientesExistente.addColumn("Nome");
		lisTableIngredientesExistente.addColumn("Calorias");
		lisTableIngredientesExistente.addColumn("Desc");
		try {
			// assume that all objects were all properly defined
			String addNome;
			String addCalorias;
			String addDesc;
			stmt = conn.createStatement();
			result = stmt.executeQuery("SELECT * FROM ingrediente ORDER BY nome ASC");
			while (result.next()) {
				addNome = result.getString("nome");
				addCalorias = result.getString("calorias");
				addDesc = result.getString("desc");
				lisTableIngredientesExistente.addRow(new Object[] { addNome, addCalorias, addDesc });
			}
		} catch (Exception err) {
			System.out.println(err);
		}
		tableIngredientesExistente.setModel(lisTableIngredientesExistente);
		tableIngredientesExistentes.setModel(lisTableIngredientesExistente);
		tableEditIngredientesExistentes.setModel(lisTableIngredientesExistente);
	}

	/** M�todo para carregar os ingredientes escolhidos para cada receita para a tabela dos ingredientes escolhidos */
	
	public void CarregarTabelaEditIngredientesEscolhidos() {
		DefaultTableModel listTableEditIngredientesEscolhidos = new DefaultTableModel();
		try {

			listTableEditIngredientesEscolhidos.addColumn("Nome");
			listTableEditIngredientesEscolhidos.addColumn("Calorias");
			listTableEditIngredientesEscolhidos.addColumn("Desc");
			listTableEditIngredientesEscolhidos.addColumn("Qtd");
			listTableEditIngredientesEscolhidos.addColumn("Unidade");

			String addNomeIngrediente;
			int addCalorias;
			String addDesc;
			int addQtd;
			String addUnidade;

			int row = tableReceitas.getSelectedRow();
			String addNomeReceita = tableReceitas.getModel().getValueAt(row, 0).toString();

			stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			result = stmt.executeQuery("SELECT * FROM detalhesReceita WHERE nomeReceita = '" + addNomeReceita + "'");

			while (result.next()) {
				System.out.println("result");
				addNomeIngrediente = result.getString("nomeIngrediente");
				addQtd = Integer.parseInt(result.getString("qtd"));
				addUnidade = result.getString("unidade");

				ResultSet result2 = stmt2.executeQuery("SELECT * FROM ingrediente where nome = '" + addNomeIngrediente + "'");
				while (result2.next()) {
					System.out.println(addNomeIngrediente);
					addCalorias = Integer.parseInt(result2.getString("calorias"));
					addDesc = result2.getString("desc");

					listTableEditIngredientesEscolhidos.addRow(new Object[] { addNomeIngrediente, addCalorias, addDesc, addQtd, addUnidade });
				}
			}

		} catch (Exception err) {
			System.out.println(err);
			System.out.println("aqui");
		}
		tableEditIngredientesEscolhidos.setModel(listTableEditIngredientesEscolhidos);
	}

	/** M�todo para criar as colunas e adicionar as linhas com as receitas existentes na base de dados */
	
	// Preencher a tableReceitasPesquisa com os dados da BD
	public void CarregarTabelaReceitas() {
		DefaultTableModel listTableReceitas = new DefaultTableModel();

		listTableReceitas.addColumn("Nome");
		listTableReceitas.addColumn("Calorias");
		listTableReceitas.addColumn("N� Doses");
		listTableReceitas.addColumn("Prepara��o (m)");
		listTableReceitas.addColumn("Confe��o (m)");
		listTableReceitas.addColumn("Classifica��o");
		listTableReceitas.addColumn("Tipo");
		tableReceitas.setDefaultEditor(Object.class, null);

		try {
			// assume that all objects were all properly defined
			String addNomeReceita;
			String addTipoReceita;
			int addNDoses;
			int addTempoPreparacao;
			int addTempoConficao;
			int addCalorias;
			int addClassificacao;

			stmt = conn.createStatement();
			result = stmt.executeQuery("SELECT * FROM receita ORDER BY nomeReceita ASC");
			while (result.next()) {
				addNomeReceita = result.getString("nomeReceita");
				addCalorias = Integer.parseInt(result.getString("caloriasReceita"));
				addNDoses = Integer.parseInt(result.getString("nDoses"));
				addTempoPreparacao = Integer.parseInt(result.getString("tempoPreparacao"));
				addTempoConficao = Integer.parseInt(result.getString("tempoConfecao"));
				addClassificacao = Integer.parseInt(result.getString("classificacao"));
				addTipoReceita = result.getString("nomeTipoReceita");

				listTableReceitas.addRow(new Object[] { addNomeReceita, addCalorias, addNDoses, addTempoPreparacao,
						addTempoConficao, addClassificacao, addTipoReceita });
			}
		} catch (Exception err) {
			System.out.println(err);
		}
		tableReceitas.setModel(listTableReceitas);
		tableReceitaPesquisa.setModel(listTableReceitas);
	}

	/** M�todo para apagar/limpar os campos do painel para adicionar receitas */
	
	// Limpa os campos do painel addReceita
	public void LimparTabelaReceitas() {
		textFieldNomeReceita.setText("");
		textFieldNDosesReceitas.setText("");
		textFieldDuracaoReceita.setText("");
		textFieldConfecaoReceita.setText("");
		textFieldQtd.setText("");

		DefaultTableModel tableIngredientesEscolhidosModels = (DefaultTableModel) tableIngredientesEscolhidos
				.getModel();

		while (tableIngredientesEscolhidosModels.getRowCount() > 0) {
			for (int i = 0; i < tableIngredientesEscolhidosModels.getRowCount(); i++) {
				tableIngredientesEscolhidosModels.removeRow(i);
			}
		}
	}

	/// 
	/// M�todo para apagar/limpar os campos do painel para editar receitas 
	///
	
	// Limpa os campos do painel editReceita
	public void LimparEditTabelaReceitas() {
		textFieldEditNomeReceita.setText("");
		textFieldEditNDosesReceita.setText("");
		textFieldEditDuracaoReceita.setText("");
		textFieldEditConfecaoReceita.setText("");
		textFieldEditQtd.setText("");

		DefaultTableModel tableEditIngredientesEscolhidosModels = (DefaultTableModel) tableEditIngredientesEscolhidos
				.getModel();

		while (tableEditIngredientesEscolhidosModels.getRowCount() > 0) {
			for (int i = 0; i < tableEditIngredientesEscolhidosModels.getRowCount(); i++) {
				tableEditIngredientesEscolhidosModels.removeRow(i);
			}
		}
	}

	/** M�todo para apagar/limpar os campos do painel dos ingredientes */
	
	public void LimparTabelaIngredientes() {
		textFieldNomeIngrediente.setText("");
		textFieldCaloriasIngrediente.setText("");
		textFieldDescricaoIngrediente.setText("");
	}
}
